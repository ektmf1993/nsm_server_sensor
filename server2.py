# -*- coding: utf-8 -*-
import socket
from thread import start_new_thread,exit
connList={}
sensorID=0
def receive(sock):#서버가 접속자와 통신하기 위한 규칙이 적용된 함수
    global sensorID
    conn,client=sock.accept()#요청을 저장하는 queue에 데이터가 추가될때까지 기다렸다가 요청이 오면 핸들을 conn에 접속자 정보를 client에 저장한다.
    connList[client[0]]='sensor'+str(sensorID)
    sensorID+=1
    start_new_thread(receive,(sock,))#다른 접속자의 요청을 받기위해 receive를 재귀호출한다.
    print client#접속자 정보를 출력한다.
    conn.send(connList[client[0]])
    while 1:#무한히 반복한다.
        try:
            fileName=conn.recv(128)#접속자로부터 통신으로 받아들일 파일의 이름을 받아 fileName에 저장한다.
            while fileName=='':
                conn.send('[@re]')
                fileName=conn.recv(128)
            conn.send('1')#파일이름에 대한 정보를 받았음을 알리고 다음 통신규칙을 이행할 것을 요청한다.
            f=open(fileName,"wb")#접속자로부터 받은 파일이름을 기반으로 파일을 생성하고 핸들을 f에 저장한다.
            while 1:#무한히 반복한다.
                data=conn.recv(1048576)#수신큐에서 1MB 만큼의 데이터를 읽어와 data에 저장한다.
                if data[-6:]=="[@EOF]":#데이터의 마지막 6글자가 [@EOF] 일 경우 True 아니면 False. 현재 수신한 파일을 다받았을때.
                    f.write(data[:-6])#데이터의 마지막 6글자를 제외하고 나머지를 파일에 저장한다.
                    print fileName,'finish'#파일을 받아오는데 완료했음을 출력한다.
                    f.close()#현재 다루고있던 파일에 대한 핸들이 담긴 f를 닫는다.
                    conn.send('1')#서버에게 다음 파일에 대한 정보를 보낼 것을 요청한다.
                    break#다음 파일을 받기위해 현재루프에서 탈출한다.
                f.write(data)#data에 저장되어있는 데이터를 f에 핸들되어있는 파일에 쓴다.
        except Exception as e:
            print connList[client[0]]+"와의 접속이 끊어졌습니다."
            conn.close()
            exit()
    conn.close()#소켓핸들에 대한 객체가 담긴 conn을 닫는다.

port=int(raw_input('PORT>'))
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)#통신할 소켓에 대한 객체를 sock에 저장한다.
sock.bind(('',port))#어드레스와 포트번호를 소켓과 연관되게 묶어준다
sock.listen(10)#외부로부터 들어오는 접속요청을 저장할 queue의 최대크기를 10으로 제한한다.
receive(sock)#receive 함수를 호출한다.
