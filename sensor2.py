# -*- coding: utf-8 -*-
from time import sleep
from os import remove, getcwd
from thread import start_new_thread
from time import sleep
from subprocess import call
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
import socket

queueList=[]#보낼 파일에 대한 정보를 담은 Queue
sensorID=''
prePacket=''#create를 기반으로 탐지하기 위한 temp 변수로 현재 create 발생 전에 발생한 create 된 파일의 정보를 prePacket에 저장한다.
def send(host,port):#매개변수로 받은 어드레스와 포트를 기반으로 접속대상에 연결하고 파일을 받을 통신규칙을 이행한다.
    sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)#통신에 사용할 소켓을 sock에 저장한다.
    sock.connect((host,port))#connect 함수의 매개변수로 넣은 어드레스와 포트를 기반으로 연결을 요청하고 sock에 소켓핸들을 저장한다.
    sensorID=sock.recv(128)
    start_new_thread(call,(['tshark','-i','eth0','-w',getcwd()+'/'+str(sensorID)+'.pcap','-b','filesize:1024','-f', 'not port '+str(port)],))#subprocess의 call함수에 실행할 명렁어에 대한 정보를 매개변수로 생성된 스레드에 담아 실행시킨다.
    while 1:#무한히 반복한다
        while len(queueList)==0:#queueList가 비어있으면 무한히 반복한다.
            sleep(1)#1초간 멈춘다.
        fileDir=queueList.pop()#queueList에서 가장 최근에 들어온 파일정보를 fileDir에 저장한다
        f=open(fileDir,"rb")#파일정보를 기반으로 파일을 열고 관련 핸들을 f에 저장한다.
        sock.send(fileDir[fileDir.rfind('/')+1:])#서버에게 파일이름에 대한 정보를 보낸다.
        while sock.recv(10)=='[@re]':#서버가 파일이름에 대한 정보를 받았음을 알릴때까지 기다린다.
            sock.send(fileDir[fileDir.rfind('/')+1:])
        while 1:#무한히 반복한다.
             data=f.read()#연 파일에 대한 핸들이 담긴 f를 통해 데이터를 읽어서 data에 저장한다
             if data=='':#읽어들인 데이터가 없을때. 즉 다 읽어들였을 경우 True 아니면 False
                 sock.send("[@EOF]")#데이터를 다보냈음을 알리는 식별자를 서버에게 전송한다.
                 print '\n',fileDir,"finish"#현재 전송하던 파일을 다보냈음을 출력한다.
                 f.close()#현재 읽어들이고 있던 파일에 대한 핸들이 담긴 f를 닫는다.
                 remove(fileDir)#다 읽어들인 파일을 삭제한다.
                 sock.recv(1)#서버에서 다음 파일을 받아들일 준비가 되었음을 알릴때까지 정지한다.
                 break#다음 파일 정보를 받아오기 위해 현재루프에서 탈출한다.
             sock.sendall(data)#data에 저장된 데이터를 서버에게 보낸다.
    sock.close()#소켓핸들에 대한 객체가 담긴 sock을 닫는다.
    
class PCAPDetection(PatternMatchingEventHandler):#pcap 파일을 탐지하기 위한 비동기적 이벤트 핸들러에 대한 class
    patterns = ["*.pcap"]#탐지할 대상파일에 대한 정보를 patterns에 저장한다. pcap이라는 확장자를 가진 모든 파일
    def process(self, event):#탐지에 성공하면 실행할 동작
        global prePacket
        print event.src_path, event.event_type#탐지된 파일에 대한 정보를 출력한다.
        if prePacket != '':#prePacket이 ''가 아니면 True 맞으면 False. 이전에 탐지된적이 없다면(첫번째 탐지라면) prePacket은 ''를 값으로 가진다.
            queueList.insert(0,prePacket)#queueList에 prePacket(바로 전에 탐지된 파일의 전체경로)의 값을 인덱스 0에 삽입한다.
        prePacket = event.src_path#현재 파일의 전체경로를 prePacket에 저장한다.

    def on_created(self, event):#create를 탐지한다.
        self.process(event)#class 내부의 process 함수를 호출한다.

ip=raw_input("IP>")#어드레스를 입력받는다.
port=raw_input("PORT>")#포트를 입력받는다.
start_new_thread(send,(ip,int(port),))#send함수에 ip와 port를 정수로 변환한 값을 매개변수로 주고 생성된 스레드에 담아 실행시킨다.
observer = Observer()
observer.schedule(PCAPDetection(), path=getcwd())#탐지할 대상에 대한 정보를 observer에 저장한다. Observer() 호출로 개체생성후 schedule함수에 탐지할 이벤트에 대한 개체와 탐지가 적용되는 경로를 매개변수로 준다.
observer.start()#observer에 저장된 정보를 바탕으로 탐지를 시작한다.
try:#Exception 탐지
    while True:#무한히 반복한다
        sleep(1)#1초간 멈춘다.
except KeyboardInterrupt:#keyboard로 Interrupt가 들어오면 runtime 오류를 내지 않고 다음을 실행시킨다.
    observer.stop()#observer에 저장된 탐지핸들을 닫는다.
observer.join()#observer가 끝날때까지 기다린다.
